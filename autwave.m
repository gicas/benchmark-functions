function W = autwave(x)
% AUTWAVE returns the right-hand side of a trapped wave eqn.
%   AUTWAVE(X) returns a function handle than represents
%   the time-dependent right-hand side of the trapped WE:
%   u''tt = u''xx - (x^2 + eps*cos(delta*t)*x^2),
%   where X is the spacial grid.

h = abs(x(1)-x(2));
r = length(x);
W = laplacian_pbc(h, r) - diag(x.^2);
% for interface compatibility
W= -W;

% end of function
end