function L = laplacian_pbc(h, r)
% LAPLACIAN_PBC returns the discretized Laplacian for a 2nd-order PDE.
%   LAPLACIAN_PBC(H,R) returns the discretized Laplacian matrix of dim=R
%   and spatial step H.

L = diag(-2*ones(r, 1), 0) + diag(ones(r-1, 1), -1) + diag(ones(r-1, 1), 1);
L(1, end) = 1;
L(end, 1) = 1;
L = L / (h * h);

% end of function
end