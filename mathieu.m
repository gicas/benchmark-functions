function M = mathieu(omega, eps)
% MATHIEU returns the right-hand side of Mathieu differential eqn.
%   MATHIEU(omega, eps, r) returns the right-hand side of the Mathieu eqn.:
%   x''(t) + M(t)x(t) = 0 ~
%   ~ x''(t) = -M(t)x(t).

M = -(omega * omega + eps * cos(2.0*t));

% end of function
end

