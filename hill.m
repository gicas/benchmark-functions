function M = hill(omega, eps, r)
% HILL returns the right-hand side of Hill matrix differential eqn.
%   HILL(omega, eps, r) returns the right-hand side of the matrix Hill eqn.:
%   x''(t) + M(t)x(t) = 0 ~
%   ~ x''(t) = -M(t)x(t).

Id = eye(r);

P = pascal(r);
% Renormalized to avoid Inf and NaN
P = P / (norm(P, 2)^0.95);
% Used to create matrices of comparable norms
np = norm(P, 2);

A = omega * omega * Id * np;

B1 = eps * Id * np;
B2 = 0.1 * eps * Id * np;

M = @(t) - (P + A + B1 * cos(2*t) + B2 * cos(4*t));

% end of function
end